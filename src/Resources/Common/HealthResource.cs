﻿namespace ProSkive.Lib.Api.Resources.Common
{
    public static class HealthStatus
    {
        public const string Healthy = "healthy";
    }
    
    public class HealthResource
    {
        public string Status { get; set; }

        public HealthResource(string status)
        {
            Status = status;
        }
    }
}