﻿namespace ProSkive.Lib.Api.Resources.Common
{
    public class InfoResource
    {
        public string Name { get; set; }

        public InfoResource(string name)
        {
            Name = name;
        }
    }
}