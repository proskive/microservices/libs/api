﻿using System;

namespace ProSkive.Lib.Api.Resources.Errors
{
    public class ErrorApiResource
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public DateTime Time { get; set; }

        public ErrorApiResource()
        {
            
        }
        
        public ErrorApiResource(int code, string message)
        {
            Code = code;
            Message = message;
            Time = DateTime.Now;
        }
    }
}