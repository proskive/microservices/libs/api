﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ProSkive.Lib.Api.Resources.Errors
{
    public class BadRequestApiResource : ErrorApiResource
    {
        public Dictionary<string, string> Errors { get; set; }
        
        public BadRequestApiResource() : base(400, "Your request was not valid.")
        {
            
        }

        public BadRequestApiResource(string message) : base(400, message)
        {
            
        }

        public BadRequestApiResource(ModelStateDictionary modelState) : base(400, "Your request contained some errors.")
        {
            if (modelState.ErrorCount < 1)
                return;
            
            Errors = new Dictionary<string, string>();
            foreach (var ms in modelState)
            {
                var message = ms.Value.Errors.FirstOrDefault()?.ErrorMessage;
                Errors.Add(ms.Key, message ?? "There is an error");
            }
        }
    }
}