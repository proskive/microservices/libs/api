﻿namespace ProSkive.Lib.Api.Resources.Errors
{
    public class InternalServerErrorApiResource : ErrorApiResource
    {
        public InternalServerErrorApiResource() : base(500, "Internal Server Error.")
        {
            
        }

        public InternalServerErrorApiResource(string message) : base(500, message)
        {
            
        }
    }
}