﻿namespace ProSkive.Lib.Api.Resources.Errors
{
    public class ResourceNotFoundApiResource : ErrorApiResource
    {
        public ResourceNotFoundApiResource() : base(404, "Resource was not found.")
        {
        }
        
        public ResourceNotFoundApiResource(string message) : base(404, message)
        {
        }
    }
}