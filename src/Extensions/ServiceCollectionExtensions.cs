﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using ProSkive.Lib.Api.Resources.Errors;

namespace ProSkive.Lib.Api.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void ConfigureApiBehavior(this IServiceCollection services)
        {
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var badRequestResource = new BadRequestApiResource("Your resource(s) contained some errors. Please refer to the errors property for additional details.");
                    return new BadRequestObjectResult(badRequestResource)
                    {
                        ContentTypes = { "application/problem+json", "application/problem+xml" }
                    };
                };
            });
        }
    }
}